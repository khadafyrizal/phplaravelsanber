<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create(){
        return view('adminlte.page.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required|max:2',
            'bio' => 'required',
        ]);
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );
    }
    public function index(){
        $cast = DB::table('cast')->get();
        return view('adminlte.page.index', compact('cast'));
    }
    public function show($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('adminlte.page.show', compact('cast'));
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('adminlte.page.edit', compact('cast'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required|max:2',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
