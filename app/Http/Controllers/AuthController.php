<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }
    public function welcome(){
        return view('welcome');
    }
    public function welcome_post(Request $request){
        $nama = $request['Firstname']." ".$request['Lastname'];
        return view('welcome', ['nama' => $nama]);
    }
}
