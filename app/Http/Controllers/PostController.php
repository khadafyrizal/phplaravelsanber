<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PostController extends Controller
{
    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([ // berfungsi melakukan validasi jika title kosong atau title tidak boleh sama dengan data yang telah terinput maka akan menampilkan error dan jika body inputan kosong maka akan menampilkan error
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);
        $query = DB::table('post')->insert([  // berfungsi melakukan insert data pada kolom title dan body berdasarkan request inputan attribute name ‘title’ dan ‘body’ seperti pada sintax sql INSERT INTO post (title, body)
            "title" => $request["title"],      // VALUES ($request[‘title’], $request[‘body’]);
            "body" => $request["body"]         // setelah proses insert berhasil maka akan menuju ke URL /post    
        ]); 
        return redirect('/posts');
    }
    public function index()
    {
        $post = DB::table('post')->get(); //berfungsi mengambil semua ada pada table post fungsi seperti pada sintax sql “SELECT * FROM post”
        return view('post.index', compact('post')); //berfungsi menreturn menuju resources/views/post/index.blade.php beserta melempar variabel post
    }
    public function show($id)
    {
        $post = DB::table('post')->where('id', $id)->first(); //Berfungsi memanpilkan data berdasarkan $id yang terdapat pada parameter public funtion show($id) sintax sqlnya  SELECT * FROM post WHERE id = $id
        return view('post.show', compact('post'));
    }
    
    public function edit($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.edit', compact('post'));
        //$post berfungsi membuat variabel yang menampung fungsi mengambil semua data berdasarkan paramenter variabel di pada public function edit($id) sintax sqlnya SELECT * FROM post WHERE id = $id dan menreturn menuju resources/views/post/edit.blade.php beserta melempar variabel post
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);
        $query = DB::table('post')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);                             //berfungsi melakukan update pada inputan attribute name title dan body berdasarkan id pada kolom database sintax sql
            return redirect('/posts');      // UPDATE post
                                            // SET title = $request[‘title], body= $request[‘body’]
                                            // WHERE id = $id
        
    }
    public function destroy($id)
    {
        $query = DB::table('post')->where('id', $id)->delete();
        return redirect('/posts');
    } 
    // berfungsi menghapus data pada table post berdasarkan $id
    // sintax sqlnya
    // DELETE FROM post WHERE id = $id

}

