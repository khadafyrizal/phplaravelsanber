<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home');

Route::get('/welcome', 'AuthController@welcome');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@welcome_post');

route::get('/master', function (){
    return view('adminlte/master');
});

Route::get('/items', function(){
    return view('items/index');
});

Route::get('/items/create', function(){
    return view('items/create');
});

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-tables', function(){
    return view('data-tables');
});

Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');


// Route::get('/post', 'PostController@index');
// Route::get('/post/create', 'PostController@create');
// Route::post('/post', 'PostController@store');
// Route::get('/post/{post_id}', 'PostController@show');
// Route::get('/post/{post_id}/edit', 'PostController@edit');
// Route::put('/post/{post_id}', 'PostController@update');
// Route::delete('/post/{post_id}', 'PostController@destroy');


