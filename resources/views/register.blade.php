<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>
        
        <form action="/welcome" method="POST">
        @csrf
            <div class="element-form">
                <label for="Fname">First Name <br><br></label> 
                <span><input type="text" name="Firstname" id="fName"></span>
            </div>
            <br>
            <div class="element-form">
                <label for="Lname">Last Name <br><br></label>
                <span><input type="text" name="Lastname" id="LName"></span>
            </div>
        
            <br>
            <div class="element-form">
                <label for="Gender">Gender :</label><br>
                <span><input type="radio" name="Gender" id="gender"> Man</span><br>
                <span><input type="radio" name="Gender" id="gender"> Women</span><br>
                <span><input type="radio" name="Gender" id="gender"> Other</span><br>
            </div>
            <br>
            <div class="element-form">
                <label for="Nationality">Nationality</label>
                <span><select name="Kewarganegaraan" id="kewarganegaraan">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Arab Saudi">Arab</option>
                    <option value="China">China</option>
                    <option value="India">India</option>
                </select></span>
                <br><br>
                <div class="element-form">
                    <label for="Language">Language Spoken</label><br><br>
                    <span><input type="checkbox" name="Bahasa" id="bahasa"> Bahasa Indonesia</span><br>
                    <span><input type="checkbox" name="Bahasa" id="bahasa"> English</span><br>
                    <span><input type="checkbox" name="Bahasa" id="bahasa"> Arabic</span><br>
                    <span><input type="checkbox" name="Bahasa" id="bahasa"> Japanesse</span><br>
                </div><br>
                <div class="element-form">
                    <label for="Bio">Bio</label><br>
                <span><textarea name="biodata" id="bio" cols="30" rows="10"></textarea></span>
                <div class="element-form">
                    <span><input type="submit" name="submit" id="submit" value="Sign Up"></span>
                </div>
            </div>
        </div>   
        </form>
    </div>
</body>
</html>